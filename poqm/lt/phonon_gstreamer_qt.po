# Lithuanian translations for phonon_gstreamer package.
#
# Andrius Štikonas <andrius@stikonas.eu>, 2009.
# Tomas Straupis <tomas.straupis@itreefinance.com>, 2011.
# Liudas Ališauskas <liudas.alisauskas@gmail.com>, 2011.
# Remigijus Jarmalavičius <remigijus@jarmalavicius.lt>, 2011.
# Liudas Alisauskas <liudas@akmc.lt>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: phonon_gstreamer\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-07-01 01:43+0200\n"
"PO-Revision-Date: 2013-01-05 16:10+0200\n"
"Last-Translator: Liudas Alisauskas <liudas@akmc.lt>\n"
"Language-Team: Lithuanian <kde-i18n-lt@kde.org>\n"
"Language: lt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=(n==1 ? 0 : n%10>=2 && (n%100<10 || n"
"%100>=20) ? 1 : n%10==0 || (n%100>10 && n%100<20) ? 2 : 3);\n"
"X-Generator: Lokalize 1.5\n"
"X-Qt-Contexts: true\n"

#: gstreamer/backend.cpp:220
#, fuzzy
#| msgid ""
#| "Warning: You do not seem to have the package gstreamer0.10-plugins-good "
#| "installed.\n"
#| "          Some video features have been disabled."
msgctxt "Phonon::Gstreamer::Backend|"
msgid ""
"Warning: You do not seem to have the package gstreamer1.0-plugins-good "
"installed.\n"
"          Some video features have been disabled."
msgstr ""
"Įspėjimas: panašu, kad neturite įsidiegę paketo gstreamer0.10-plugins-good.\n"
"          Išjungtos kai kurios vaizdo atgaminimo galimybės."

#: gstreamer/backend.cpp:228
#, fuzzy
#| msgid ""
#| "Warning: You do not seem to have the base GStreamer plugins installed.\n"
#| "          All audio and video support has been disabled"
msgctxt "Phonon::Gstreamer::Backend|"
msgid ""
"Warning: You do not seem to have the base GStreamer plugins installed.\n"
"          All audio and video support has been disabled"
msgstr ""
"Įspėjimas: panašu, kad neturite įsidiegę pagrindinių GStreamer priedų.\n"
"          Išjungtas bet koks garso ir vaizdo palaikymas"

#: gstreamer/mediaobject.cpp:428
#, fuzzy
#| msgid "Default"
msgctxt "Phonon::Gstreamer::MediaObject|"
msgid "Default"
msgstr "Numatyta"

#: gstreamer/mediaobject.cpp:442 gstreamer/mediaobject.cpp:475
#, fuzzy
#| msgid "Unknown"
msgctxt "Phonon::Gstreamer::MediaObject|"
msgid "Unknown"
msgstr "Nežinoma"

#: gstreamer/mediaobject.cpp:462
#, fuzzy
#| msgid "Disable"
msgctxt "Phonon::Gstreamer::MediaObject|"
msgid "Disable"
msgstr "Išjungti"

#: gstreamer/pipeline.cpp:515
#, fuzzy
#| msgid "One or more plugins are missing in your GStreamer installation."
msgctxt "Phonon::Gstreamer::Pipeline|"
msgid "One or more plugins are missing in your GStreamer installation."
msgstr "Trūksta vieno ar daugiau priedų jūsų GStreamer įdiegime."

#: gstreamer/plugininstaller.cpp:188
#, fuzzy
#| msgid "Missing codec helper script assistant."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Missing codec helper script assistant."
msgstr "Nerastas garso koduočių pagalbinių scenarijų asistentas."

#: gstreamer/plugininstaller.cpp:190
#, fuzzy
#| msgid "Plugin codec installation failed."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Plugin codec installation failed."
msgstr "Nepavyko įdiegti koduotės priedo."

#: gstreamer/plugininstaller.cpp:214
#, fuzzy
#| msgid "Phonon attempted to install an invalid codec name."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Phonon attempted to install an invalid codec name."
msgstr "Phonon bandė įdiegti nekorektišką koduotės pavadinimą."

#: gstreamer/plugininstaller.cpp:217
#, fuzzy
#| msgid "The codec installer crashed."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "The codec installer crashed."
msgstr "Koduotės įdiegimas sulūžo."

#: gstreamer/plugininstaller.cpp:220
#, fuzzy
#| msgid "The required codec could not be found for installation."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "The required codec could not be found for installation."
msgstr "Nerasta įdiegimui reikalinga koduotė."

#: gstreamer/plugininstaller.cpp:223
#, fuzzy
#| msgid "An unspecified error occurred during codec installation."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "An unspecified error occurred during codec installation."
msgstr "Įdiegiant koduotę įvyko nenurodyta klaida."

#: gstreamer/plugininstaller.cpp:226
#, fuzzy
#| msgid "Not all codecs could be installed."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Not all codecs could be installed."
msgstr "Nevisas koduotes pavyko įdiegti."

#: gstreamer/plugininstaller.cpp:229
#, fuzzy
#| msgid "User aborted codec installation"
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "User aborted codec installation"
msgstr "Naudotojas nutraukė koduotės įdiegimą."

#: gstreamer/plugininstaller.cpp:240
#, fuzzy
#| msgid "Could not update plugin registry after update."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Could not update plugin registry after update."
msgstr "Po atnaujinimo nepavyko atnaujinti priedo registrų."
