# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# Chetan Khona <chetan@kompkin.com>, 2013.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2019-07-01 01:43+0200\n"
"PO-Revision-Date: 2013-02-09 16:14+0530\n"
"Last-Translator: Chetan Khona <chetan@kompkin.com>\n"
"Language-Team: American English <kde-i18n-doc@kde.org>\n"
"Language: mr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Lokalize 1.5\n"
"X-Qt-Contexts: true\n"

#: gstreamer/backend.cpp:220
msgctxt "Phonon::Gstreamer::Backend|"
msgid ""
"Warning: You do not seem to have the package gstreamer1.0-plugins-good "
"installed.\n"
"          Some video features have been disabled."
msgstr ""

#: gstreamer/backend.cpp:228
msgctxt "Phonon::Gstreamer::Backend|"
msgid ""
"Warning: You do not seem to have the base GStreamer plugins installed.\n"
"          All audio and video support has been disabled"
msgstr ""

#: gstreamer/mediaobject.cpp:428
#, fuzzy
#| msgid "Default"
msgctxt "Phonon::Gstreamer::MediaObject|"
msgid "Default"
msgstr "मूलभूत"

#: gstreamer/mediaobject.cpp:442 gstreamer/mediaobject.cpp:475
#, fuzzy
#| msgid "Unknown"
msgctxt "Phonon::Gstreamer::MediaObject|"
msgid "Unknown"
msgstr "अपरिचीत"

#: gstreamer/mediaobject.cpp:462
#, fuzzy
#| msgid "Disable"
msgctxt "Phonon::Gstreamer::MediaObject|"
msgid "Disable"
msgstr "अकार्यान्वित करा"

#: gstreamer/pipeline.cpp:515
#, fuzzy
#| msgid "One or more plugins are missing in your GStreamer installation."
msgctxt "Phonon::Gstreamer::Pipeline|"
msgid "One or more plugins are missing in your GStreamer installation."
msgstr "तुमच्या जि-स्ट्रीमर प्रतिष्ठापनात एक किंवा अधिक प्लगइन्स सापडले नाहीत. "

#: gstreamer/plugininstaller.cpp:188
#, fuzzy
#| msgid "Missing codec helper script assistant."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Missing codec helper script assistant."
msgstr "कोडेक मदत स्क्रिप्ट सहायक सापडत नाही."

#: gstreamer/plugininstaller.cpp:190
#, fuzzy
#| msgid "Plugin codec installation failed."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Plugin codec installation failed."
msgstr "प्लगइन कोडेक प्रतिष्ठापन अपयशी."

#: gstreamer/plugininstaller.cpp:214
#, fuzzy
#| msgid "Phonon attempted to install an invalid codec name."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Phonon attempted to install an invalid codec name."
msgstr "फोनॉनने अवैध नावाच्या कोडेकच्या प्रतिष्ठापनेचा प्रयत्न केला."

#: gstreamer/plugininstaller.cpp:217
#, fuzzy
#| msgid "The codec installer crashed."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "The codec installer crashed."
msgstr "कोडेक प्रतिष्ठापन क्रॅश झाले."

#: gstreamer/plugininstaller.cpp:220
#, fuzzy
#| msgid "The required codec could not be found for installation."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "The required codec could not be found for installation."
msgstr "आवश्यक असलेले कोडेक प्रतिष्ठापनाकरिता सापडले नाही."

#: gstreamer/plugininstaller.cpp:223
#, fuzzy
#| msgid "An unspecified error occurred during codec installation."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "An unspecified error occurred during codec installation."
msgstr "कोडेक प्रतिष्ठापन करताना त्रुटी आढळली."

#: gstreamer/plugininstaller.cpp:226
#, fuzzy
#| msgid "Not all codecs could be installed."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Not all codecs could be installed."
msgstr "सर्व कोडेक्स प्रतिष्ठापीत करू शकत नाही."

#: gstreamer/plugininstaller.cpp:229
#, fuzzy
#| msgid "User aborted codec installation"
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "User aborted codec installation"
msgstr "कोडेक प्रतिष्ठापन वापरकर्त्याने बंद केले"

#: gstreamer/plugininstaller.cpp:240
#, fuzzy
#| msgid "Could not update plugin registry after update."
msgctxt "Phonon::Gstreamer::PluginInstaller|"
msgid "Could not update plugin registry after update."
msgstr "अद्ययावतीकरणा नंतर प्लगइनची रजिस्ट्री अद्ययावत करू शकत नाही."
